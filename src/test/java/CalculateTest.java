import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import static junit.framework.TestCase.*;


public class CalculateTest {
    @BeforeClass
    public static void beforeClass(){
        System.out.println("\nBefore class executed.");
    }
    @Before
    public void before(){
        System.out.println("\nTesting yet another method:");
    }
    @Test
    public void testSum(){
        assertEquals(18, new Calculate().sum(9,9));
        System.out.println("Sum is ok.");
    }
    @Test
    public void testSub(){
        assertEquals(15, new Calculate().sub(20,5));
        System.out.println("Sub is ok.");
    }
    @Test
    public void testMul(){
        assertTrue("Expected = actual", 25==new Calculate().mul(5,5));
        System.out.println("Mul is ok.");
    }
    @Test
    public void testDiv(){
        assertTrue("Expected = actual", 5==new Calculate().div(25,5));
        System.out.println("Div is ok.");
    }
    @AfterClass
    public static void afterClass(){
        System.out.println("\nAll tests have finished successfully");
    }
}
